<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConstructionShop extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'country',
        'street_address',
        'city',
        'state',
        'zip',
        'contact',
        'email',
        'website',
        'about',
        'services',
        'gallery'
    ];
}
