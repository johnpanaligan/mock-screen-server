<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ConstructionShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'company_name' => $this->company_name,
            'country' => $this->country,
            'street_address' => $this->street_address,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip,
            'contact' => $this->contact,
            'email' => $this->email,
            'website' => $this->website,
            'about' => $this->about,
            'services' => $this->services,
            'gallery' => $this->gallery,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
