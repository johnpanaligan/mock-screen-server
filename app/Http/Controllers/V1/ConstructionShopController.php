<?php

namespace App\Http\Controllers\V1;

use App\Models\ConstructionShop;
use App\Http\Requests\StoreConstructionShopRequest;
use App\Http\Requests\UpdateConstructionShopRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\ConstructionShopResource;

class ConstructionShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConstructionShopResource::collection(ConstructionShop::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreConstructionShopRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreConstructionShopRequest $request)
    {
        $constructionShop = ConstructionShop::create($request->all());
        
        return new ConstructionShopResource($constructionShop);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ConstructionShop  $constructionShop
     * @return \Illuminate\Http\Response
     */
    public function show(ConstructionShop $constructionShop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateConstructionShopRequest  $request
     * @param  \App\Models\ConstructionShop  $constructionShop
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConstructionShopRequest $request, ConstructionShop $constructionShop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConstructionShop  $constructionShop
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConstructionShop $constructionShop)
    {
        //
    }
}
