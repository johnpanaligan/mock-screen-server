<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_shops', function (Blueprint $table) {
            $table->id();
            $table->string('company_name', 255);
            $table->string('country', 100);
            $table->string('street_address', 100);
            $table->string('city', 100);
            $table->string('state', 100);
            $table->string('zip', 25);
            $table->string('contact', 25);
            $table->string('email', 100);
            $table->string('website', 100)->nullable();
            $table->text('about')->nullable();
            $table->json('services')->nullable();
            $table->json('gallery')->nullable();
            $table->timestamps();
            $table->foreignIdFor(User::class, 'user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_shops');
    }
};
